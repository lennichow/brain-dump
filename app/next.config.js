/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  experimental: {
    images: {
      unoptimized: true,
    },
  },

  basePath: process.env.BASE_PATH,
  assetPrefix: process.env.BASE_PATH,
};

module.exports = nextConfig;
