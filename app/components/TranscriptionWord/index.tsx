import { Button, Box, Text, Tooltip } from "@chakra-ui/react";
import React from "react";

type Props = {
  isStartOfSentence: boolean;
  isEndOfSentence: boolean;
  isActive: boolean;
  colorScheme: string;
  timestamp: number;
  word: string;
  audioRef: any;
  rawLine: string;
  accuracyB: number;
};

// eslint-disable-next-line react/display-name
const TranscriptionWord = React.memo(
  ({
    isStartOfSentence,
    isEndOfSentence,
    isActive,
    colorScheme,
    timestamp,
    word,
    audioRef,
    rawLine,
    accuracyB,
  }: Props) => {
    const inaccurateStyle =
      accuracyB < 0.4
        ? {
            textDecorationStyle: "wavy",
            textDecorationThickness: 1,
            textDecorationLine: "underline",
          }
        : {};

    return (
      <Tooltip label={rawLine}>
        <Box
          display="inline-flex"
          flexDirection="column"
          alignItems="center"
          marginBottom={2}
          marginLeft={isStartOfSentence ? 1 : 0}
          marginRight={isEndOfSentence ? 1 : 0}
        >
          <Button
            colorScheme={colorScheme}
            isActive={isActive}
            textTransform={isStartOfSentence ? "capitalize" : undefined}
            px={0.5}
            py={0}
            variant="ghost"
            transition="none"
            onClick={() =>  audioRef.current.currentTime = timestamp}
            {...inaccurateStyle}
          >
            {word}
          </Button>
          <Text
            as="small"
            size="xs"
            opacity={0.5}
            fontSize={10}
            fontWeight={300}
          >
            {timestamp}
          </Text>
        </Box>
      </Tooltip>
    );
  }
);

export default TranscriptionWord;
