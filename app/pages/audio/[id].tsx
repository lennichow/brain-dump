import {
  Button,
  ButtonGroup,
  Container,
  Heading,
  Alert,
  AlertIcon,
  AlertTitle,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  // Tooltip,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import Head from "next/head";
import { useEffect, useMemo, useRef, useState } from "react";
import fs from "fs";
import TranscriptionWord from "../../components/TranscriptionWord";
import { parseCtmLine } from "../../utils/parseCtmLine";

type Params = {
  id: string;
};

type Props = {
  fileName: string;

  CtmSpeak: Array<{
    rawLine: string;
    parsedLine: ReturnType<typeof parseCtmLine>;
  }>;

  audioUrl: string;
};

import { GetStaticProps, GetStaticPaths } from "next";

export const getStaticPaths: GetStaticPaths = async () => {
  const files = fs.readdirSync("../audio");
  return {
    paths: files.map((file) => ({ params: { id: file } })),
    fallback: false, // can also be true or 'blocking'
  };
};

export const getStaticProps: GetStaticProps<Props, Params> = async (
  context
) => {
  const ctmSpeakPath = `../output/${context.params.id}/1Best.ctm.spk`;

  const audioUrl = `${process.env.BASE_PATH ?? ''}/static/audio/${context.params.id}`;

  return {
    props: {
      fileName: context.params.id,

      CtmSpeak: fs.existsSync(ctmSpeakPath)
        ? fs
            .readFileSync(ctmSpeakPath, "utf8")
            .split("\n")
            .map((rawLine) => ({ rawLine, parsedLine: parseCtmLine(rawLine) }))
            .filter(({ parsedLine }) => !!parsedLine.word)
        : [],

      audioUrl,
    },
  };

  // ...
};

const colorSchemesForCtmLine = [
  "red",
  "orange",
  "blue",
  "purple",
  "pink",
  "yellow",
];

const Audio: NextPage = ({ fileName, CtmSpeak, audioUrl }: Props) => {
  const audioRef = useRef<HTMLAudioElement>();
  const [progress, setProgress] = useState<number>();

  useEffect(() => {
    const currentAudioRef = audioRef.current

    const create = async () => {
      if (audioRef.current) {
        audioRef.current.addEventListener("timeupdate", updateTime);
      }
    };

    const updateTime = () => {
      const currentTime = audioRef.current.currentTime

      setProgress(currentTime);
    }

    create();

    return () => {
      if (currentAudioRef) {
        currentAudioRef.removeEventListener("timeupdate", updateTime)
      }
    };
  }, [audioRef]);

  const handlePlayPause = () => {
    if (audioRef.current.paused) {
    audioRef.current.play();
    } else {
    audioRef.current.pause();
    }
  };

  const handleBackButtonPress = () => {
    forwardTime(-15)
  };

  const handleForwardButtonPress = () => {
    forwardTime(15)
  };

  const forwardTime = (time :number) => {
    audioRef.current.currentTime += time
  }

  const currentActiveSpeak = useMemo(() => {
    return CtmSpeak.findIndex((v) => v.parsedLine.timestamp > progress) - 1;
  }, [progress, CtmSpeak]);

  return (
    <Container py={8} maxWidth="80ch">
      <Head>
        <title>{fileName} - Audio file</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <audio controls autoPlay style={{ width: '100%' }} ref={audioRef}>
         <source src={audioUrl} />
      </audio>

      <ButtonGroup className="controls" isAttached>
        <Button onClick={handleBackButtonPress}>Back 15</Button>
        <Button onClick={handlePlayPause}>{"Play / pause"}</Button>
        <Button onClick={handleForwardButtonPress}>Forward 15</Button>
      </ButtonGroup>

      <Slider aria-label="slider-ex-4" defaultValue={30}>
        <SliderTrack bg="red.100">
          <SliderFilledTrack bg="tomato" />
        </SliderTrack>
        <SliderThumb></SliderThumb>
      </Slider>

      <Heading size="lg" marginBottom={4}>
        Transcription
      </Heading>
      {CtmSpeak.length === 0 && (
        <Alert
          status="error"
          variant="subtle"
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          textAlign="center"
          py={4}
        >
          <AlertIcon boxSize="40px" mr={0} />
          <AlertTitle mt={4} mb={1} fontSize="lg">
            No transcription found
          </AlertTitle>
        </Alert>
      )}
      {CtmSpeak.map(({ rawLine, parsedLine }, i, arr) => {
        const isActive = currentActiveSpeak === i;
        const isStartOfSentence =
          i === 0 ||
          arr[i - 1].parsedLine.sentenceGroup !== parsedLine.sentenceGroup;

        const isEndOfSentence =
          i === arr.length - 1 ||
          arr[i + 1].parsedLine.sentenceGroup !== parsedLine.sentenceGroup;

        return (
          <TranscriptionWord
            rawLine={rawLine}
            key={`word-${i}`}
            isStartOfSentence={isStartOfSentence}
            isEndOfSentence={isEndOfSentence}
            colorScheme={
              colorSchemesForCtmLine[
                parsedLine.intSentenceGroup % colorSchemesForCtmLine.length
              ]
            }
            isActive={isActive}
            timestamp={parsedLine.timestamp}
            word={parsedLine.word}
            audioRef={audioRef}
            accuracyB={parsedLine.accuracyB}
          />
        );
      })}
    </Container>
  );
};

export default Audio;
