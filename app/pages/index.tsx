import type { GetStaticProps, NextPage } from "next";
import fs from "fs";
import Head from "next/head";
// import { WaveSurfer } from 'wavesurfer-react'
import {
  getCtmKeywords,
  getCtmSpeakPath,
  parseCtmLine,
} from "../utils/parseCtmLine";
import { IAudioMetadata, parseFile } from "music-metadata";

import {
  Badge,
  Heading,
  Link,
  Highlight,
  Tag,
  Flex,
  Container,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
} from "@chakra-ui/react";

import NextLink from "next/link";
import { getDurationFromSeconds, parseDateFromString } from "../utils/date";
import { addSeconds, differenceInHours, isSameDay, parseJSON } from "date-fns";

type File = {
  fileName: string;
  metadata: IAudioMetadata;
  CtmSpeak: Array<{
    rawLine: string;
    parsedLine: ReturnType<typeof parseCtmLine>;
  }>;
  keywords: Array<{
    word: string;
    count: number;
  }>;
  date?: Date;
  groupId: number;
};

type Group = {
  groupId: number;
  filesLength: number;
  startDate: string;
  endDate: string;
};
export type HomeProps = {
  files: Array<File>;
  groups: Array<Group>;
};

const AUDIO_PATH = "../audio";
export const getStaticProps: GetStaticProps<HomeProps> = async (context) => {
  const files = fs.readdirSync(AUDIO_PATH);
  const loadedFiles = await Promise.all(
    files.map(async (file) => {
      const ctmSpeakPath = getCtmSpeakPath(file);

      const CtmSpeak = fs.existsSync(ctmSpeakPath)
        ? fs
            .readFileSync(ctmSpeakPath, "utf8")
            .split("\n")
            .map((rawLine) => ({
              rawLine,
              parsedLine: parseCtmLine(rawLine),
            }))
            .filter(({ parsedLine }) => !!parsedLine.word)
        : [];

      const metadata = await parseFile(AUDIO_PATH + "/" + file);

      delete metadata.native;

      return {
        fileName: file,
        metadata,
        CtmSpeak,
        keywords: getCtmKeywords(CtmSpeak.map((lines) => lines.parsedLine)),

        date: parseDateFromString(file),
      };
    })
  );

  /* @ts-ignore */
  loadedFiles.sort((a, b) => parseJSON(a.date) - parseJSON(b.date));
  loadedFiles.reverse();

  const filesWithGrouping = loadedFiles.reduce((prevVal, file) => {
    const lastFile = prevVal[prevVal.length - 1] as File | undefined;

    const fileWithGroup = { ...file } as any;
    if (!lastFile) {
      fileWithGroup.groupId = 0;
    } else {
      if (
        differenceInHours(parseJSON(lastFile.date), parseJSON(file.date)) < 4
      ) {
        fileWithGroup.groupId = lastFile.groupId;
      } else {
        fileWithGroup.groupId = lastFile.groupId + 1;
      }
    }

    return [...prevVal, fileWithGroup];
  }, []);

  return {
    props: {
      files: filesWithGrouping,
      groups: [
        ...Array(filesWithGrouping[filesWithGrouping.length - 1]?.groupId ?? 0),
      ].map((e, groupId) => {
        const files = filesWithGrouping.filter(
          (file) => file.groupId === groupId
        );

        return {
          groupId,
          filesLength: files.length,
          startDate: files[files.length - 1].date,
          endDate: JSON.stringify(
            addSeconds(
              parseJSON(files[0].date),
              files[0].metadata?.format.duration
            )
          ),
        };
      }),
    },
  };

  // ...
};

const Home: NextPage = ({ files, groups }: HomeProps) => {
  return (
    <>
      <Head>
        <title>{files.length} bestanden - Audiolog van Robin</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container maxWidth="120ch">
        <Accordion
          allowMultiple
          allowToggle
          defaultIndex={groups.map(({ groupId }) => groupId)}
        >
          {groups.map(({ groupId, startDate, endDate, filesLength }) => (
            <AccordionItem key={groupId}>
              <h2>
                <AccordionButton>
                  <Flex flex="1" textAlign="left" gap={4}>
                    <span>
                      <strong>📅 Start: </strong>

                      {new Intl.DateTimeFormat(undefined, {
                        dateStyle: "full",
                        timeStyle: "short",
                      }).format(parseJSON(startDate))}
                    </span>
                    <span>
                      <strong>Eind: </strong>
                      <span>
                        {new Intl.DateTimeFormat(undefined, {
                          dateStyle: isSameDay(
                            parseJSON(startDate),
                            parseJSON(endDate)
                          )
                            ? undefined
                            : "full",
                          timeStyle: "short",
                        }).format(parseJSON(endDate))}
                      </span>
                    </span>
                    <span> 🎙️ {filesLength}</span>
                  </Flex>
                  <AccordionIcon />
                </AccordionButton>
              </h2>
              <AccordionPanel pb={4}>
                {files
                  .filter((file) => file.groupId === groupId)
                  .map((file) => (
                    <NextLink
                      href={`/audio/${file.fileName}`}
                      passHref
                      key={file.fileName}
                    >
                      <Link as="a" marginBottom={8} display="block">
                        <Flex alignItems="center" gap={4}>
                          <Heading size="base">{file.fileName}</Heading>

                          <Tag
                            colorScheme={
                              file.metadata.format.duration > 120
                                ? "red"
                                : "white"
                            }
                          >
                            {" "}
                            🕐{" "}
                            {getDurationFromSeconds(
                              file.metadata.format.duration
                            )}
                          </Tag>
                        </Flex>
                        {file.CtmSpeak.length < 80 ? (
                          <Highlight
                            query={file.keywords.map((keyword) => keyword.word)}
                            styles={{
                              px: "1",
                              py: "0",
                              bg: "teal.500",
                              rounded: 2,
                              color: "white",
                            }}
                          >
                            {file.CtmSpeak.map(
                              (line) => line.parsedLine.word
                            ).join(" ")}
                          </Highlight>
                        ) : (
                          file.keywords.map((keyword) => (
                            <Badge
                              mr={2}
                              variant={keyword.count > 1 ? "solid" : "subtle"}
                              size="lg"
                              key={keyword.word}
                            >
                              {keyword.word} <small>x {keyword.count}</small>
                            </Badge>
                          ))
                        )}
                      </Link>
                    </NextLink>
                  ))}
              </AccordionPanel>
            </AccordionItem>
          ))}
        </Accordion>
      </Container>
    </>
  );
};

export default Home;
