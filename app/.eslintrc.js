module.exports = {
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint", "unused-imports"],
  extends: [
    "next",
    "prettier",
    // "plugin:@typescript-eslint/eslint-recommended",
    // "plugin:@typescript-eslint/recommended",
    // "react-app",
    // "react-app/jest"
  ],

  rules: { "unused-imports/no-unused-imports": "error" },
};
