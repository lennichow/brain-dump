export const getCtmSpeakPath = (file: string) => {
  return `../output/${file}/1Best.ctm.spk`;
};
export const parseCtmLine = (line: string) => {
  const splittedLine = line.split(" ");
  return {
    fileName: splittedLine[0],
    unknown: splittedLine[1],
    timestamp: Number.parseFloat(splittedLine[2]),
    accuracyA: splittedLine[3],
    word: splittedLine[4],
    accuracyB: Number.parseFloat(splittedLine[5]),
    sentenceGroup: splittedLine[6],
    intSentenceGroup: Number.parseInt((splittedLine[6] ?? "").replace("S", "")),
  };
};

import { getWordsList } from "most-common-words-by-language";

const forbiddenWords = [
  ...getWordsList("dutch", 2000),
  "zo'n",
  "m'n",
  "'m",
  "'k",
  "'t",
  "c",
  "dk",
  "sowieso",
  "effe",
];
export const getCtmKeywords = (
  ctmLines: Array<ReturnType<typeof parseCtmLine>>
) => {
  const keywords = ctmLines
    .filter((line) => !!line.word && !forbiddenWords.includes(line.word))
    .reduce((prevLines, ctmLine) => {
      const element = prevLines.find((line) => line.word === ctmLine.word);
      if (!element) {
        prevLines.push({ word: ctmLine.word, count: 1 });
        return prevLines;
      }

      element.count++;
      return prevLines;
    }, []);

  keywords.sort((a, b) => b.count - a.count);
  return keywords;
};
