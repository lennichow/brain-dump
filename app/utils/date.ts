import { formatDuration, intervalToDuration, parse } from "date-fns";

export const getDurationFromSeconds = (seconds: number) => {
  return formatDuration(
    intervalToDuration({
      start: new Date(0, 0, 0, 0, 0, 0, 0),
      end: new Date(0, 0, 0, 0, 0, seconds, 0),
    })
  );
};

export const parseDateFromString = (name: string) => {
  // Remove _01 and .wav
  const [date, time] = name.split(".")[0].split("_");
  const nameWithoutExtension = `${date}_${time}`;

  return JSON.stringify(parse(nameWithoutExtension, "yyMMdd_HHmm", new Date()));
};
