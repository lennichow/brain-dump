script_dir=$(echo dirname $0)
files=$(./scripts/update-which-files.sh)
input_dir=$(pwd)/audio

set -x

echo "Missing files:"
echo $files

cd /opt/Kaldi_NL

for file in $files
do
  echo "Decoding file $file..."
  ./decode_GN.sh $input_dir/$file $OUTPUT_DIR/$file
done

cd -
