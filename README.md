# audiolog

Abusing Gitlab and its free services for your own (Dutch) audiolog.
Transcriptions in Dutch generated with [Kaldi_NL](https://github.com/opensource-spraakherkenning-nl/Kaldi_NL).

Please note that Gitlab Pages has a limit to sizes of artifacts for deployment.

## Preparation

- Execute following manual Gitlab CI job: `build:container` just once to build to image repository. This might take some time
- (optionally) Set visibility of Gitlab Pages to whatever you prefer.
- (optionally) Increase CI timeout of Gitlab CI

## Preparation for uploading audio files to Gitlab with your computer

- Install `git` on your computer with Git LFS support
- Clone the repository

## Upload audio files

Upload your audio files to [/audio](/audio). It roughly goes like this:

```bash
# Update your files to files on remote
git pull

# -- Insert your audio files to the /audio path --

# Push them to remote
git add -A
git commit -m "some nice message here about what changed"
git push
```


## Transcribe files

After uploading files (see step above) go to gitlab.com -> your project -> CI/CD -> Pipelines -> start job `speechtotext:audio`

Wait until the job finished (takes roughly as long as the length of the audio you've sent)

